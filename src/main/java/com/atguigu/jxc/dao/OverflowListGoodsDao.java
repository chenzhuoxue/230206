package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsDao {
    void saveOverflowList(OverflowList overflowList);

    void saveOverFlowListGoods(OverflowListGoods overflowListGoods);

    List<OverflowList> findList(String sTime, String eTime);

    List<OverflowListGoods> findByDamageListId(Integer overflowListId);
}
