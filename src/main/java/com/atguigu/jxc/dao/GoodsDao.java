package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    List<Goods> listInventory(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("codeOrName") String codeOrName,
                              @Param("goodsTypeId") Integer goodsTypeId);

    Integer getGoodsCount(@Param("codeOrName") String codeOrName);


    Goods getGoodsByGoodsId(Integer goodsId);

    void updateGoods(Goods goods);

    void saveGoods(Goods goods);

    void delete(Integer goodsId);

    List<Goods> getNoInventoryQuantity(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer getNoInventoryQuantityGoodsCount(@Param("nameOrCode")String nameOrCode);

    List<Goods> getInventoryQuantity(@Param("offSet") Integer offSet,@Param("rows") Integer rows,@Param("nameOrCode") String nameOrCode);

    Integer getInventoryQuantityGoodsCount(@Param("nameOrCode")String nameOrCode);

    List<Goods> findAll();
}