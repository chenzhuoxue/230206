package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {
    List<Supplier> list(@Param("offSet") Integer offSet,
                        @Param("rows")Integer rows,
                        @Param("supplierName")String supplierName);

    Integer getSupplierCount(@Param("supplierName") String supplierName);

    void save(Supplier supplier);

    void updateSupplier(Supplier supplier);

    void delete(Integer supplierId);

    Supplier findSupplierById(Integer supplierId);
}
