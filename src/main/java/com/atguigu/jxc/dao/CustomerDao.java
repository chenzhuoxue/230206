package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {
    List<Customer> list(@Param("offSet") Integer offSet,@Param("rows") Integer rows,
                        @Param("customerName")String customerName);

    Integer findCustomerCount(String customerName);

    void update(Customer customer);

    void save(Customer customer);

    Customer findCustomerById(Integer customerId);

    void deleteById(Integer customerId);
}
