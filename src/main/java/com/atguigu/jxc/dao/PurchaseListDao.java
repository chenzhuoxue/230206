package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.PurchaseList;

import java.util.List;

public interface PurchaseListDao {
   List<PurchaseList> findBySupplierId(Integer id);

}
