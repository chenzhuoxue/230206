package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;

import java.util.List;

public interface DamageListDao {
    void save(DamageList damageList);

    List<DamageList> findList(String  sTime, String  eTime);
}
