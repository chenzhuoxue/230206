package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.OverflowList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface OverflowListGoodsService {
    public abstract ServiceVO save(OverflowList overflowList, String overflowListGoodsStr,HttpSession session);

    public abstract Map<String, Object> list(String sTime, String eTime, HttpSession session);

    public abstract Map<String, Object> goodsList(Integer overflowListId);
}
