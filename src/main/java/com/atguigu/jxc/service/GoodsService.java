package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {

    public abstract ServiceVO getCode();

    public abstract Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    public abstract Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId);

    public abstract ServiceVO saveOrUpdate(Goods goods);

    public abstract ServiceVO delete(Integer goodsId);

    public abstract Map<String, Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode);

    public abstract Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    public abstract ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,Double purchasingPrice);

    public abstract ServiceVO deleteStock(Integer goodsId);

    public abstract Map<String, Object> listAlarm();
}
