package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;

import javax.servlet.http.HttpSession;
import java.util.Map;

public interface DamageListGoodsService {
    public abstract ServiceVO save(DamageList damageList, String damageListGoodsStr,HttpSession session);

    public abstract Map<String, Object> list(String  sTime, String  eTime, HttpSession session);

    public abstract Map<String, Object> goodsList(Integer damageListId);
}
