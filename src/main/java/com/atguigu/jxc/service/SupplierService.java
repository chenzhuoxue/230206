package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {
    public  abstract Map<String, Object> list( Integer page, Integer rows, String supplierName);

    public  abstract void save(Integer supplierId, Supplier supplier);

   public abstract void deleteBatch(String ids);
}
