package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.*;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.ReturnList;
import com.atguigu.jxc.entity.SaleListGoods;
import com.atguigu.jxc.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private SaleListGoodsDao saleListGoodsDao;

    @Autowired
    private ReturnListGoodsDao returnListGoodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;

    @Autowired
    private CustomerReturnListGoodsDao customerReturnListGoodsDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    private PurchaseListGoodsDao purchaseListGoodsDao;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {

        Map<String , Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Goods> goodsList = goodsDao.listInventory(offSet,rows,codeOrName,goodsTypeId);
        goodsList= goodsList.stream().map(goods -> {
            //
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeNameByGoodsTypeId(goods.getGoodsTypeId()));
            //销售总数
            Integer goodsId = goods.getGoodsId();

            Integer saleTotal = saleListGoodsDao.getSaleTotalByGoodsId(goodsId);
            Integer returnTotal =returnListGoodsDao.getSaleTotalByGoodsId(goodsId);
            if(returnTotal != null ){
                saleTotal = saleTotal-returnTotal;
                goods.setSaleTotal(saleTotal);
            }else {
                goods.setSaleTotal(saleTotal);
                if(saleTotal == null){
                    goods.setSaleTotal(0);
                }
            }
            return goods;
        }).collect(Collectors.toList());

        Integer total  = goodsDao.getGoodsCount(codeOrName);

        map.put("rows",goodsList);
        map.put("total",total);

        return map;
    }

    /*
    分页查询所有商品信息
    */
    @Override
    public Map<String, Object> list(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {

        Map<String , Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        //查询商品的数量
        Integer total = goodsDao.getGoodsCount(goodsName);
        List<Goods> goodsList = goodsDao.listInventory(offSet, rows, goodsName, goodsTypeId);
        goodsList.forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeNameByGoodsTypeId(goods.getGoodsTypeId()));
            goods.setSaleTotal(null);
        });
        map.put("rows",goodsList);
        map.put("total",total);

        return map;
    }

    /*
    * 商品的添加或者修改*/
    @Override
    public ServiceVO saveOrUpdate(Goods goods) {
        Integer goodsId = goods.getGoodsId();
        Goods goods1 = goodsDao.getGoodsByGoodsId(goodsId);
        if(goods1 != null){
            //表示之前商品就存在，因此进行更新操作
            goodsDao.updateGoods(goods);
        }
        goods.setInventoryQuantity(50);
        goods.setState(0);
        //表示之前商品不存在进行新增操作
        goodsDao.saveGoods(goods);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO delete(Integer goodsId) {
        //根据id查询商品，判断商品的状态
        Integer state = goodsDao.getGoodsByGoodsId(goodsId).getState();
        if(state == 0 ){
            //表示商品无子节点
            //客户退货单商品列表中存在该商品
            Integer customerReturnTotalByGoodsId = customerReturnListGoodsDao.getCustomerReturnTotalByGoodsId(goodsId);
            if(customerReturnTotalByGoodsId != null ){
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
            }
            //商品报损单商品列表
            //商品报溢单商品列表
            //进货单商品列表
            PurchaseList purchase = purchaseListGoodsDao.getPurchaseListByGoodsId(goodsId);
            if(purchase != null){
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
            }
            //退货单商品列表
            ReturnList returnList = returnListGoodsDao.getReturnListByGoodsId(goodsId);
            if(returnList != null){
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
            }
            //销售单商品列表
            SaleListGoods saleListGoods = saleListGoodsDao.getSaleListByGoodsId(goodsId);
            if(saleListGoods != null){
                return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
            }

            goodsDao.delete(goodsId);
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
        }
        return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
    }

    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page,Integer rows,String nameOrCode) {

        Map<String , Object > map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(offSet,rows,nameOrCode);
        goodsList.forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeNameByGoodsTypeId(goods.getGoodsTypeId()));
            goods.setSaleTotal(null);
        });
        Integer total = goodsDao.getNoInventoryQuantityGoodsCount(nameOrCode);

        map.put("rows",goodsList);
        map.put("total",total);

        return map;
    }

    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        Map<String , Object > map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goodsList = goodsDao.getInventoryQuantity(offSet,rows,nameOrCode);
        goodsList.forEach(goods -> {
            goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeNameByGoodsTypeId(goods.getGoodsTypeId()));
            goods.setSaleTotal(null);
        });
        Integer total = goodsDao.getInventoryQuantityGoodsCount(nameOrCode);

        map.put("rows",goodsList);
        map.put("total",total);

        return map;
    }

    @Override
    public ServiceVO saveStock(Integer goodsId,Integer inventoryQuantity,Double purchasingPrice) {

        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        goods.setInventoryQuantity(inventoryQuantity);
        goods.setPurchasingPrice(purchasingPrice);
        goodsDao.updateGoods(goods);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        //根据id查询商品，判断商品的状态
        Goods goods = goodsDao.getGoodsByGoodsId(goodsId);
        Integer state = goods.getState();

        if(state != 0 ){
            return new ServiceVO(ErrorCode.GOODS_TYPE_ERROR_CODE, ErrorCode.GOODS_TYPE_ERROR_MESS);
        }

        //客户退货单商品列表中存在该商品
        Integer customerReturnTotalByGoodsId = customerReturnListGoodsDao.getCustomerReturnTotalByGoodsId(goodsId);
        if(customerReturnTotalByGoodsId != null ){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
        //商品报损单商品列表
        //商品报溢单商品列表
        //进货单商品列表
        PurchaseList purchase = purchaseListGoodsDao.getPurchaseListByGoodsId(goodsId);
        if(purchase != null){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
        //退货单商品列表
        ReturnList returnList = returnListGoodsDao.getReturnListByGoodsId(goodsId);
        if(returnList != null){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }
        //销售单商品列表
        SaleListGoods saleListGoods = saleListGoodsDao.getSaleListByGoodsId(goodsId);
        if(saleListGoods != null){
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);
        }

        goods.setInventoryQuantity(0);
        goodsDao.updateGoods(goods);

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> listAlarm() {
        Map<String , Object > map = new HashMap<>();
        List<Goods>  goodsList = goodsDao.findAll();
        List<Goods> list = new ArrayList<>();
        goodsList.forEach(goods -> {

            if(goods.getInventoryQuantity() < goods.getMinNum()){
                goods.setGoodsTypeName(goodsTypeDao.getGoodsTypeNameByGoodsTypeId(goods.getGoodsTypeId()));
                list.add(goods);
            }
        });
        map.put("rows",list);
        return map;
    }
}
