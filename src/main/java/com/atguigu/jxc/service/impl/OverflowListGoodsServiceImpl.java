package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverflowListGoodsService;
import com.atguigu.jxc.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OverflowListGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;

    @Autowired
    private UserService userService;

    @Override
    public ServiceVO save(OverflowList overflowList, String overflowListGoodsStr ,HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        overflowList.setUserId(user.getUserId());

        overflowListGoodsDao.saveOverflowList(overflowList);

        //保存溢出的商品
        Gson gson = new Gson();
        Type type = new TypeToken<List<OverflowListGoods>>() {}.getType();
       List<OverflowListGoods>  overflowListGoodsList = gson.fromJson(overflowListGoodsStr, type);

        overflowListGoodsList.forEach(overflowListGoods -> {
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            overflowListGoodsDao.saveOverFlowListGoods(overflowListGoods);
        });

        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String sTime, String eTime, HttpSession session) {
        Map<String ,Object> map = new HashMap<>();
        List<OverflowList> overflowLists = overflowListGoodsDao.findList(sTime,eTime);
        overflowLists.forEach(overflowList -> {
            String userName = (String) userService.loadUserInfo(session).get("userName");
            overflowList.setTrueName(userName);
        });
        map.put("rows",overflowLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        Map<String ,Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.findByDamageListId(overflowListId);

        map.put("rows",overflowListGoodsList);
        return map;
    }
}
