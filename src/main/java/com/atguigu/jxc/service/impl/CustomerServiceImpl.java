package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String customerName) {
        Map<String , Object> map = new HashMap<>();
        //页面的判断
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        Integer count = customerDao.findCustomerCount(customerName);
        List<Customer> customerList =  customerDao.list(offSet,rows,customerName);

        map.put("total",count);
        map.put("rows",customerList);
        return map;
    }

    @Override
    public void saveOrUpdate(Integer customerId, Customer customer) {

        Customer customer1 = customerDao.findCustomerById(customerId);
        if(customer1 != null) {
            customerDao.update(customer);
        }
        customerDao.save(customer);
    }

    @Override
    public void deleteBatch(String ids) {
        String[] split = ids.split(",");
        for (String id : split) {
            Integer customerId = Integer.valueOf(id);
            customerDao.deleteById(customerId);
        }
    }
}
