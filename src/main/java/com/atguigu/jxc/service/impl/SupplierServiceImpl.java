package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.PurchaseListDao;
import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.PurchaseList;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;

    @Autowired
    private PurchaseListDao purchaseListDao;

    @Override
    public Map<String, Object> list(Integer page, Integer rows, String supplierName) {

        Map<String , Object> map = new HashMap<>();
        //页面的判断
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;

        List<Supplier> supplierList =  supplierDao.list(offSet,rows,supplierName);
        Integer count = supplierDao.getSupplierCount(supplierName);

        map.put("total",count);
        map.put("rows",supplierList);
        return map;
    }

    @Override
    public void save(Integer supplierId, Supplier supplier) {
        Supplier supplier1 = supplierDao.findSupplierById(supplierId);

        if(supplier1 != null){
            supplierDao.updateSupplier(supplier);
        }
        supplierDao.save(supplier);
    }

    @Override
    public void deleteBatch(String ids) {
        String[] idStr = ids.split(",");
        for (String id : idStr) {
            Integer supplierId = Integer.valueOf(id);
            if(purchaseListDao.findBySupplierId(supplierId) != null){
                supplierDao.delete(supplierId);
            }
        }

    }

}
