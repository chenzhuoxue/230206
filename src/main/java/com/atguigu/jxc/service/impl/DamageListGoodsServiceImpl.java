package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.UserDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.UserService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;

    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private UserService userService;


    @Override
    public ServiceVO save(DamageList damageList, String damageListGoodsStr,HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        damageList.setUserId(user.getUserId());

        damageListDao.save(damageList);

        Gson gson = new Gson();
        //网上查询到的 解决返回多个对象的问题
        Type listType = new TypeToken<List<DamageListGoods>>() {}.getType();

        ArrayList<DamageListGoods> damageListGoodsList = gson.fromJson(damageListGoodsStr, listType);
        damageListGoodsList.forEach(damageListGoods -> {
            damageListGoods.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.save(damageListGoods);
        });
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @Override
    public Map<String, Object> list(String  sTime, String  eTime, HttpSession session) {
        Map<String ,Object> map = new HashMap<>();
        List<DamageList> damageLists = damageListDao.findList(sTime,eTime);
        damageLists.forEach(damageList -> {
            String userName = (String) userService.loadUserInfo(session).get("userName");
            damageList.setTrueName(userName);
        });
        map.put("rows",damageLists);
        return map;
    }

    @Override
    public Map<String, Object> goodsList(Integer damageListId) {
        Map<String ,Object> map = new HashMap<>();
        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.findByDamageListId(damageListId);

        map.put("rows",damageListGoodsList);
        return map;
    }
}
