package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;

import java.util.Map;

public interface CustomerService {
    public abstract Map<String, Object> list(Integer page, Integer rows, String customerName);

    public abstract void saveOrUpdate(Integer customerId, Customer customer);

    public abstract void deleteBatch(String ids);
}
