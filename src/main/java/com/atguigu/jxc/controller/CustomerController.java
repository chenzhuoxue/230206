package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PostMapping("/list")
    public Map<String,Object> list(Integer page, Integer rows, String  customerName){
        return customerService.list(page,rows,customerName);
    }

    @PostMapping("/save")
    public ServiceVO save( Integer customerId, Customer customer){
        customerService.saveOrUpdate(customerId,customer);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }

    @PostMapping("/delete")
    public ServiceVO delete(String  ids){
        customerService.deleteBatch(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }


}
