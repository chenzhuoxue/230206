package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;

    @PostMapping("/save")
    public ServiceVO save(DamageList damageList, String damageListGoodsStr,HttpSession session){
        return damageListGoodsService.save(damageList,damageListGoodsStr,session);
    }

    @PostMapping("/list")
    public Map<String,Object> list (String  sTime, String  eTime, HttpSession session){
        return damageListGoodsService.list(sTime,eTime,session);
    }

    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(Integer damageListId){
        return damageListGoodsService.goodsList(damageListId);
    }

}
