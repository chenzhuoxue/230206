package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;

    @PostMapping("/list")
    public Map<String,Object>  list(Integer page,Integer rows, String supplierName){
        return supplierService.list(page,rows,supplierName);
    }

    @PostMapping("/save")
    public ServiceVO save( Integer supplierId, Supplier supplier){
        supplierService.save(supplierId,supplier);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }
    @PostMapping("/delete")
    public ServiceVO deleteBatch(String ids){
        supplierService.deleteBatch(ids);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);
    }




}

